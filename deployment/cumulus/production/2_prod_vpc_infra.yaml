# Production Services VPC Infrastructure:
#
#   The Production Services VPC serves as an isolated environment for Internet facing production services.
#   It is peer connected to the hub VPC, allowing routing of traffic between VPC networks, but is not
#   directly routable to or from the on-premisis network to reduce exposure.
#
#   This Cumulus template manages the deployment and configuration of the following resources:
#     - Virtual Private Cloud (VPC)
#     - Internet Gateway (IGW)
#     - VPC Peering Connection
#     - DHCP Options
#     - Subnets
#     - NATs
#     - Security groups
#

# Overall stack name. All stacks in CF get prefixed with this name.
cloud-hub:
    # The region Cumulus will create the stack in.
    region: eu-west-1
    highlight-output: true
    stacks:
        # Stack to create VPC and IGW
        prod-vpc-igw:
            cf_template: ../../VPC.template
            depends:
            params:
                # CIDR block for the entire VPC; ensure it does not conflict with
                # that of the on-premises network.
                VPCCIDR:
                    value: 172.30.64.0/22
                ResourceLabel:
                    value: prod

        # Stack to create VPC Peering Connection with the Hub VPC.
        peer-connect:
            cf_template: ../../PeerConnect.template
            depends: 
                - prod-vpc-igw
            params: 
                VPC:
                    source: prod-vpc-igw
                    type: resource
                    variable: VPC
                VPCPeer:
                    source: vpc-igw
                    type: resource
                    variable: VPC
                ResourceLabel:
                    value: prod
             
        # Stack to create DHCP Options to bootstrap domain clients.
        # This may need to be updated as new domain controllers are spun up.
        prod-dhcp-options:
            cf_template: ../../DHCPOptions.template
            depends:
                - prod-vpc-igw
            # For the following parameter we are utilizing the IP addresses of the DCs
            # in the Hub VPC.
            params:
                DomainDNSName:
                    value: pv.com 
                DomainDNSServers:
                    value: 172.30.62.12,172.30.62.140,AmazonProvidedDNS
                NetbiosNameServers:   
                    value: 172.30.62.12,172.30.62.140
                NTPServers: 
                    value: 172.30.62.12,172.30.62.140
                VPC:
                    source: prod-vpc-igw
                    type: resource
                    variable: VPC
                ResourceLabel:
                    value: prod

        # Stack to create the Security Groups to allow boxes on the private subnets to communicate
        # with the outside world through the NAT machines.
        prod-nat-security-groups:
            cf_template: ../../NatSecurityGroups.template
            depends:
                - prod-vpc-igw
            params:
                # CIDR block whitelisted for SSH access to NAT machines. Defaults to private subnets only.
                NATSSHCIDR:
                    value: 172.30.66.0/23
                # Private subnet for availability zone 1
                SubnetCIDR1:
                    value: 172.30.66.0/25
                # Private subnet for availability zone 2
                SubnetCIDR2:
                    value: 172.30.66.128/25
                # Private subnet for availability zone 3
                SubnetCIDR3:                
                    value: 172.30.67.0/25
                # Private subnet for availability zone 4, should it exist.
                SubnetCIDR4:
                    value: None
                VPC:
                    source: prod-vpc-igw
                    type: resource
                    variable: VPC
                ResourceLabel:
                    value: prod

        # Create subnets and routes for AZ 1
        prod-zone-1:
            cf_template: ../../ZoneCreate.template
            depends:
                - prod-nat-security-groups
            params:
                VPC:
                    source: prod-vpc-igw
                    type: resource
                    variable: VPC
                InternetGatewayId:
                    source: prod-vpc-igw
                    type: output
                    variable: InternetGatewayId                
                NATSecurityGroup:
                    source: prod-nat-security-groups
                    type: output
                    variable: NATSG
                KeyPairName:
                    value: ops
                NATInstanceType:
                    value: t2.small
                # 0-based index of AWS region's AZ list
                AvailabilityZone:  
                    value: 0
                DMZSubCIDR:
                    value: 172.30.64.0/25
                PrivSubCIDR:
                    value: 172.30.66.0/25
                ResourceLabel:
                    value: prod

        # Create subnets and routes for AZ 2
        prod-zone-2:
            cf_template: ../../ZoneCreate.template
            depends:
                - prod-nat-security-groups
            params:
                VPC:
                    source: prod-vpc-igw
                    type: resource
                    variable: VPC
                InternetGatewayId:
                    source: prod-vpc-igw
                    type: output
                    variable: InternetGatewayId                
                NATSecurityGroup:
                    source: prod-nat-security-groups
                    type: output
                    variable: NATSG
                KeyPairName:
                    source: zone-1
                    type: parameter
                    variable: KeyPairName
                NATInstanceType:
                    source: zone-1
                    type: parameter
                    variable: NATInstanceType
                # 0-based index of AWS region's AZ list
                AvailabilityZone:
                    value: 1
                DMZSubCIDR:
                    value: 172.30.64.128/25
                PrivSubCIDR:
                    value: 172.30.66.128/25
                ResourceLabel:
                    value: prod

        # Create subnets and routes for AZ 3
        prod-zone-3:
            cf_template: ../../ZoneCreate.template
            depends:
                - prod-nat-security-groups
            params:
                VPC:
                    source: prod-vpc-igw
                    type: resource
                    variable: VPC
                InternetGatewayId:
                    source: prod-vpc-igw
                    type: output
                    variable: InternetGatewayId                
                NATSecurityGroup:
                    source: prod-nat-security-groups
                    type: output
                    variable: NATSG
                KeyPairName:
                    source: zone-1
                    type: parameter
                    variable: KeyPairName
                NATInstanceType:
                    source: zone-1
                    type: parameter
                    variable: NATInstanceType
                # 0-based index of AWS region's AZ list
                AvailabilityZone:
                    value: 2
                DMZSubCIDR:
                    value: 172.30.65.0/25
                PrivSubCIDR:
                    value: 172.30.67.0/25
                ResourceLabel:
                    value: prod

        # Update routing in zone-1 of both VPCs for peering
        peer-zone-1:
            cf_template: ../../PeerRouting.template
            depends:
                - prod-zone-1
                - peer-connect
            params:
                VPC1RT:
                    source: prod-zone-1
                    type: resource
                    variable: PrivateRouteTable
                VPC1CIDR:
                    source: prod-nat-security-groups
                    type: parameter
                    variable: NATSSHCIDR
                VPC2RT:
                    source: zone-1
                    type: resource
                    variable: PrivateRouteTable
                VPC2CIDR:
                    source: vpc-igw
                    type: parameter
                    variable: VPCCIDR
                PeerConnect:
                    source: peer-connect
                    type: output
                    variable: PeerConnect

        # Update routing in zone-2 of both VPCs for peering
        peer-zone-2:
            cf_template: ../../PeerRouting.template
            depends:
                - prod-zone-2
                - peer-connect
            params:
                VPC1RT:
                    source: prod-zone-2
                    type: resource
                    variable: PrivateRouteTable
                VPC1CIDR:
                    source: prod-nat-security-groups
                    type: parameter
                    variable: NATSSHCIDR
                VPC2RT:
                    source: zone-2
                    type: resource
                    variable: PrivateRouteTable
                VPC2CIDR:
                    source: vpc-igw
                    type: parameter
                    variable: VPCCIDR
                PeerConnect:
                    source: peer-connect
                    type: resource
                    variable: PeerConnect

        # Update routing in zone-3 of both VPCs for peering
        peer-zone-3:
            cf_template: ../../PeerRouting.template
            depends:
                - prod-zone-3
                - peer-connect
            params:
                VPC1RT:
                    source: prod-zone-3
                    type: resource
                    variable: PrivateRouteTable
                VPC1CIDR:
                    source: prod-nat-security-groups
                    type: parameter
                    variable: NATSSHCIDR
                VPC2RT:
                    source: zone-3
                    type: resource
                    variable: PrivateRouteTable
                VPC2CIDR:
                    source: vpc-igw
                    type: parameter
                    variable: VPCCIDR
                PeerConnect:
                    source: peer-connect
                    type: resource
                    variable: PeerConnect
