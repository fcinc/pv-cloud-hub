# Cloud Hub VPC Infrastructure:
#
#   The Cloud Hub VPC serves as an extension of the on-premises network and mimics the layout of other
#   physical locations. Private subnets are VPN connected to the private networks of the on-premsis
#   location as well as the in-region production services VPC.
#
#   This Cumulus template manages the deployment and configuration of the following resources:
#     - Virtual Private Cloud (VPC)
#     - Internet Gateway (IGW)
#     - Virtual Private Gateway (VPG)
#     - DHCP Options
#     - Subnets
#     - NATs
#     - Security groups
#

# Overall stack name. All stacks in CF get prefixed with this name.
cloud-hub:
    # The region Cumulus will create the stack in.
    region: eu-west-1
    highlight-output: true
    stacks:
        # Stack to create VPC and IGW
        vpc-igw:
            cf_template: ../../VPC.template
            depends:
            params:
                # CIDR block for the entire VPC; ensure it does not conflict with
                # that of the on-premises network.
                VPCCIDR:
                    value: 172.30.60.0/22
                ResourceLabel:
                    value: hub

        # Stack to create VPG and VPC attachment.
        vpg-attachment:
            cf_template: ../../VPG.template
            depends: 
                - vpc-igw
            params: 
                VPC:
                    source: vpc-igw
                    type: resource
                    variable: VPC
                ResourceLabel:
                    value: hub
             
        # Stack to create DHCP Options to bootstrap domain clients.
        # This may need to be updated as new domain controllers are spun up.
        dhcp-options:
            cf_template: ../../DHCPOptions.template
            depends:
                - vpc-igw
            # For the following parameter we are utilizing the IP addresses of the DCs we will be
            # spinning up.
            params:
                DomainDNSName:
                    value: pv.com 
                DomainDNSServers:
                    value: 172.30.62.12,172.30.62.140,AmazonProvidedDNS
                NetbiosNameServers:   
                    value: 172.30.62.12,172.30.62.140
                NTPServers: 
                    value: 172.30.62.12,172.30.62.140
                VPC:
                    source: vpc-igw
                    type: resource
                    variable: VPC
                ResourceLabel:
                    value: hub

        # Stack to create the Security Groups to allow boxes on the private subnets to communicate
        # with the outside world through the NAT machines.
        nat-security-groups:
            cf_template: ../../NatSecurityGroups.template
            depends:
                - vpc-igw
            params:
                # CIDR block whitelisted for SSH access to NAT machines. Defaults to private subnets only.
                NATSSHCIDR:
                    value: 172.30.62.0/23
                # Private subnet for availability zone 1
                SubnetCIDR1:
                    value: 172.30.62.0/25
                # Private subnet for availability zone 2
                SubnetCIDR2:
                    value: 172.30.62.128/25
                # Private subnet for availability zone 3
                SubnetCIDR3:                
                    value: 172.30.63.0/25
                # Private subnet for availability zone 4, should it exist.
                SubnetCIDR4:
                    value: None
                VPC:
                    source: vpc-igw
                    type: resource
                    variable: VPC
                ResourceLabel:
                    value: hub

        # Create subnets and routes for AZ 1
        zone-1:
            cf_template: ../../ZoneCreate.template
            depends:
                - nat-security-groups
            params:
                VPC:
                    source: vpc-igw
                    type: resource
                    variable: VPC
                InternetGatewayId:
                    source: vpc-igw
                    type: output
                    variable: InternetGatewayId                
                NATSecurityGroup:
                    source: nat-security-groups
                    type: output
                    variable: NATSG
                KeyPairName:
                    value: ops
                NATInstanceType:
                    value: t2.small
                # 0-based index of AWS region's AZ list
                AvailabilityZone:  
                    value: 0
                DMZSubCIDR:
                    value: 172.30.60.0/25
                PrivSubCIDR:
                    value: 172.30.62.0/25
                ResourceLabel:
                    value: hub

        # Create subnets and routes for AZ 2
        zone-2:
            cf_template: ../../ZoneCreate.template
            depends:
                - nat-security-groups
            params:
                VPC:
                    source: vpc-igw
                    type: resource
                    variable: VPC
                InternetGatewayId:
                    source: vpc-igw
                    type: output
                    variable: InternetGatewayId                
                NATSecurityGroup:
                    source: nat-security-groups
                    type: output
                    variable: NATSG
                KeyPairName:
                    source: zone-1
                    type: parameter
                    variable: KeyPairName
                NATInstanceType:
                    source: zone-1
                    type: parameter
                    variable: NATInstanceType
                # 0-based index of AWS region's AZ list
                AvailabilityZone:
                    value: 1
                DMZSubCIDR:
                    value: 172.30.60.128/25
                PrivSubCIDR:
                    value: 172.30.62.128/25
                ResourceLabel:
                    value: hub

        # Create subnets and routes for AZ 3
        zone-3:
            cf_template: ../../ZoneCreate.template
            depends:
                - nat-security-groups
            params:
                VPC:
                    source: vpc-igw
                    type: resource
                    variable: VPC
                InternetGatewayId:
                    source: vpc-igw
                    type: output
                    variable: InternetGatewayId                
                NATSecurityGroup:
                    source: nat-security-groups
                    type: output
                    variable: NATSG
                KeyPairName:
                    source: zone-1
                    type: parameter
                    variable: KeyPairName
                NATInstanceType:
                    source: zone-1
                    type: parameter
                    variable: NATInstanceType
                # 0-based index of AWS region's AZ list
                AvailabilityZone:
                    value: 2
                DMZSubCIDR:
                    value: 172.30.61.0/25
                PrivSubCIDR:
                    value: 172.30.63.0/25
                ResourceLabel:
                    value: hub
