{
    "AWSTemplateFormatVersion" : "2010-09-09",
    "Description" : "This template creates an empty VPC, with an Internet Gateway attached.",
    "Parameters" : {
        "VPCCIDR" : {
            "Description" : "CIDR Block for the VPC",
            "Type" : "String",
            "Default" : "10.0.0.0/16",
            "AllowedPattern" : "[a-zA-Z0-9]+\\..+"
        },
        "ResourceLabel" : {
            "Description" : "Label for inclusion in resource naming",
            "Type" : "String",
            "Default" : "hub",
            "AllowedPattern" : "[a-zA-Z0-9]+"
        }
    },
    "Resources" : {
        "VPC" : {
            "Type" : "AWS::EC2::VPC",
            "Properties" : {
                "CidrBlock" : {
                    "Ref" : "VPCCIDR"
                },
                "Tags" : [
                    {
                        "Key" : "Application",
                        "Value" : {
                            "Ref" : "AWS::StackName"
                        }
                    },
                    {
                        "Key" : "Name",
                        "Value" : { "Fn::Join" : [
                          "-",
                          [
                              {
                                  "Ref" : "ResourceLabel"
                              },
                              "vpc"
                          ]
                        ] }
                    }
                ]
            }
        },
        "InternetGateway"           : {
            "Type" : "AWS::EC2::InternetGateway",
            "Properties" : {
                "Tags" : [
                    {
                        "Key" : "Application",
                        "Value" : {
                            "Ref" : "AWS::StackName"
                        }
                    },
                    {
                        "Key" : "Name",
                        "Value" : { "Fn::Join" : [
                          "-",
                          [
                              {
                                  "Ref" : "ResourceLabel"
                              },
                              "igw"
                          ]
                        ] }
                    }
                ]
            }
        },
        "AttachGateway"             : {
            "Type" : "AWS::EC2::VPCGatewayAttachment",
            "Properties" : {
                "VpcId" : {
                    "Ref" : "VPC"
                },
                "InternetGatewayId" : {
                    "Ref" : "InternetGateway"
                }
            }
        }
	},
    "Outputs" : {
        "VPC" : {
            "Value" : {
                "Ref" : "VPC"
            },
            "Description" : "VPC ID"
        },
        "VPCCIDR" : {
            "Value" : {
                "Ref" : "VPCCIDR"
            },
            "Description" : "VPC CIDR"
        },
        "InternetGatewayId" : {
            "Value" : {
                "Ref" : "InternetGateway"
			},
            "Description" : "Internet Gateway ID"
        }		
    }
}
