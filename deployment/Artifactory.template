{
  "AWSTemplateFormatVersion" : "2010-09-09",
  "Description" : "This template deploys a JFrog Artifactory application stack",
  "Parameters" : {
    "VPC" : {
      "Description" : "The VPC where the security groups will be created",
      "Type" : "AWS::EC2::VPC::Id"
    },
    "DMZSubnet0" : {
      "Description" : "DMZ Subnet Id 0",
	  "Type" : "AWS::EC2::Subnet::Id"
    },
    "DMZSubnet1" : {
      "Description" : "DMZ Subnet Id 1",
	  "Type" : "AWS::EC2::Subnet::Id"
    },
    "DMZSubnet2" : {
      "Description" : "DMZ Subnet Id 2",
	  "Type" : "AWS::EC2::Subnet::Id"
    },
    "PrivateSubnet0" : {
      "Description" : "Private Subnet Id 0",
	  "Type" : "AWS::EC2::Subnet::Id"
    },
    "PrivateSubnet1" : {
      "Description" : "Private Subnet Id 1",
	  "Type" : "AWS::EC2::Subnet::Id"
    },
    "PrivateSubnet2" : {
      "Description" : "Private Subnet Id 2",
	  "Type" : "AWS::EC2::Subnet::Id"
    },
    "OnPremisesCIDR" : {
      "Description" : "CIDR Block for the on premises network, which should be connected via VPN or AWS Direct Connect",
      "Type" : "String",
      "Default" : "10.10.0.0/16",
      "AllowedPattern" : "[a-zA-Z0-9]+\\..+"
    },
    "PrivNetCIDR" : {
      "Description" : "CIDR Block for the Private Subnet",
      "Type" : "String",
      "Default" : "10.10.128.0/17",
      "AllowedPattern" : "[a-zA-Z0-9]+\\..+"
    },
    "KeyPairName" : {
      "Description" : "Public/private key pairs allow you to securely connect to your instance after it launches",
      "Type" : "AWS::EC2::KeyPair::KeyName"
    },
    "InstanceType" : {
      "Description" : "Amazon EC2 instance type for the Artifactory instance",
      "Type" : "String",
      "Default" : "c4.large",
      "AllowedValues" : [
        "c4.large",
        "c4.xlarge"
      ]
    },
    "InstanceAMI" : {
      "Description" : "Amazon Machine Image (AMI) for the Artifactory instance",
      "Type" : "String",
      "AllowedPattern" : "ami-[0-9a-f]*"
    },
    "InstanceEBS" : {
      "Description" : "Elastic Block Storage (EBS) snapshot ID for the Artifactory application data",
      "Type" : "String",
      "AllowedPattern" : "snap-[0-9a-f]*"
    },
    "DBInstanceType" : {
      "Description" : "Amazon EC2 instance type for the Artifactory RDS instance",
      "Type" : "String",
      "Default" : "db.t2.small",
      "AllowedValues" : [
        "db.t2.small",
        "db.m3.medium"
      ]
    },
    "DBAdminPassword" : {
      "Description" : "Password for the Artifactory database",
      "Type" : "String",
      "NoEcho" : "true"
    },
    "SSLCertARN" : {
      "Description" : "Amazon Resource Name (ARN) for the SSL certificate to be utilized",
      "Type" : "String",
      "AllowedPattern" : "arn:aws:iam::\\d*:server-certificate.*"
    },
    "ResourceLabel" : {
      "Description" : "Label for inclusion in resource naming",
      "Type" : "String",
      "Default" : "hub",
      "AllowedPattern" : "[a-zA-Z0-9]+"
    }
  },
  "Resources" : {
    "BackupRole": {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "AssumeRolePolicyDocument": {
          "Version" : "2012-10-17",
          "Statement": [ {
            "Effect": "Allow",
            "Principal": {
              "Service": [ "ec2.amazonaws.com" ]
            },
            "Action": [ "sts:AssumeRole" ]
           } ]
        },
        "Path": "/",
        "Policies": [ {
          "PolicyName": "backupPolicy",
          "PolicyDocument": {
            "Version" : "2012-10-17",
            "Statement": [ 
            {
              "Action": "ec2:*",
              "Effect": "Allow",
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": "elasticloadbalancing:*",
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": "cloudwatch:*",
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": "autoscaling:*",
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": "rds:*",
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": "iam:*",
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": [
                "cloudformation:DescribeStacks",
                "cloudformation:DescribeStackEvents",
                "cloudformation:DescribeStackResource",
                "cloudformation:DescribeStackResources",
                "cloudformation:GetTemplate",
                "cloudformation:List*",
                "cloudformation:UpdateStack"
              ],
              "Resource": "*"
            } ]
          }
        } ]
      }
    },
    "BackupInstanceProfile": {
      "Type": "AWS::IAM::InstanceProfile",
      "Properties": {
        "Path": "/",
        "Roles": [ {
          "Ref": "BackupRole"
        } ]
      }
    },
    "ArtElbSG" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "JFrog Artifactory Load Balancer Security Group",
        "VpcId" : {
          "Ref" : "VPC"
        },
        "SecurityGroupIngress" : [
          {
            "IpProtocol" : "tcp",
            "FromPort" : "80",
            "ToPort" : "80",
            "CidrIp" : "0.0.0.0/0"
          },
          {
            "IpProtocol" : "tcp",
            "FromPort" : "443",
            "ToPort" : "443",
            "CidrIp" : "0.0.0.0/0"
          },
          {
            "IpProtocol" : "tcp",
            "FromPort" : "8081",
            "ToPort" : "8081",
            "CidrIp" : "0.0.0.0/0"
          }
        ],
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [
              "-",
              [
                {
                  "Ref" : "ResourceLabel"
                },
                "artifactory-elb"
              ]
            ] }
          }
        ]
      }
    },
    "ArtSG" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "JFrog Artifactory Security Group",
        "VpcId" : {
          "Ref" : "VPC"
        },
        "SecurityGroupIngress" : [
          {
            "IpProtocol" : "tcp",
            "FromPort" : "22",
            "ToPort" : "22",
            "CidrIp" : {
              "Ref" : "OnPremisesCIDR"
            }
          },
          {
            "IpProtocol" : "tcp",
            "FromPort" : "22",
            "ToPort" : "22",
            "CidrIp" : {
              "Ref" : "PrivNetCIDR"
            }
          },
          {
            "IpProtocol" : "tcp",
            "FromPort" : "8081",
            "ToPort" : "8081",
            "SourceSecurityGroupId" : {
              "Ref" : "ArtElbSG"
            }
          }
        ],
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [
              "-",
              [
                {
                  "Ref" : "ResourceLabel"
                },
                "artifactory"
              ]
            ] }
          }
        ]
      }
    },
    "ArtSGIngress" : {
      "Type" : "AWS::EC2::SecurityGroupIngress",
      "Properties" : {
        "GroupId" : {
          "Ref" : "ArtSG"
        },
        "IpProtocol" : "-1",
        "SourceSecurityGroupId" : {
          "Ref" : "ArtSG"
        }
      },
      "DependsOn" : "ArtSG"
    },
    "ArtElb" : {
      "Type" : "AWS::ElasticLoadBalancing::LoadBalancer",
      "Properties" : {
        "Subnets" : [
          {
            "Ref" : "DMZSubnet0"
          },
          {
            "Ref" : "DMZSubnet1"
          },
          {
            "Ref" : "DMZSubnet2"
          }
        ],
        "HealthCheck" : {
          "HealthyThreshold" : "5",
          "Interval" : "30",
          "Target" : "TCP:8081",
          "Timeout" : "5",
          "UnhealthyThreshold" : "2"
        },
        "SecurityGroups" : [
          {
            "Ref" : "ArtElbSG"
          }
        ],
        "Listeners" : [
          {
            "InstancePort" : "8081",
            "LoadBalancerPort" : "443",
            "Protocol" : "HTTPS",
            "InstanceProtocol" : "HTTP",
            "SSLCertificateId" : {
              "Ref" : "SSLCertARN"
            }
          },
          {
            "InstancePort" : "8081",
            "LoadBalancerPort" : "8081",
            "Protocol" : "TCP",
            "InstanceProtocol" : "TCP"
          },
          {
            "InstancePort" : "8081",
            "LoadBalancerPort" : "80",
            "Protocol" : "TCP",
            "InstanceProtocol" : "TCP"
          }
        ],
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [
              "-",
              [
                {
                  "Ref" : "ResourceLabel"
                },
                "artifactory"
              ]
            ] }
          }
        ]
      }
    },
    "ArtLC" : {
      "Type" : "AWS::AutoScaling::LaunchConfiguration",
      "Properties" : {
        "ImageId" : {
          "Ref" : "InstanceAMI"
        },
        "InstanceType" : {
          "Ref" : "InstanceType"
        },
        "KeyName" : {
          "Ref" : "KeyPairName"
        },
        "IamInstanceProfile" : {
          "Ref" : "BackupInstanceProfile"
        },
        "EbsOptimized" : true,
        "InstanceMonitoring" : "true",
        "SecurityGroups" : [
          {
            "Ref": "ArtSG"
          }
        ],
        "BlockDeviceMappings" : [
          {
            "DeviceName" : "/dev/sda1",
            "Ebs" : {
              "VolumeSize" : 8
            }
          },
          {
            "DeviceName" : "/dev/sdb",
            "Ebs": {
              "SnapshotId" : {
                "Ref" : "InstanceEBS"
              },
              "VolumeSize" : 1024
            }
          }
        ],
        "UserData" : {
          "Fn::Base64": {
            "Fn::Join": [
              "", 
              [
                "#cloud-config\n",
                "hostname: ",
                {
                  "Ref" : "ResourceLabel"
                },
                "-artifactory-0\n",
                "fqdn: ",
                {
                  "Ref" : "ResourceLabel"
                },
                "-artifactory-0.pv.com\n",
                "manage_etc_hosts: True\n",
                "runcmd:\n",
                "  - \"FACTER_hostname='prod-artifactory-0' FACTER_environment='production' FACTER_group='artifactory' FACTER_db_host='",
                {
                  "Fn::GetAtt" : [
                    "ArtRds",
                    "Endpoint.Address"
                  ]
                },
                "' puppet apply --verbose --modulepath='/home/centos/packer/module-0' --hiera_config='/home/centos/packer/hiera.yaml'",
                " --tags='update' --detailed-exitcodes /home/centos/packer/manifests/atlassian.pp\"\n",
                "  - \"sed -i -e 's/^STACK_NAME=.*$/STACK_NAME=",
                {
                  "Ref" : "AWS::StackName"
                },
                "/' /home/centos/backup/artifactory-backup.vars.sh\"\n",
                "  - \"sed -i -e 's/^MYSQL_HOST=.*$/MYSQL_HOST=",
                {
                  "Fn::GetAtt" : [
                    "ArtRds",
                    "Endpoint.Address"
                  ]
                },
                "/' /home/centos/backup/artifactory-backup.vars.sh\"\n",
                "  - \"sed -i -e 's/^PROD_FILE=.*$/PROD_FILE=artifactory-backup.vars.sh/' /home/centos/backup/backup.sh\"\n",
                "  - \"sed -i -e 's/^PROD_FILE=.*$/PROD_FILE=artifactory-backup.vars.sh/' /home/centos/backup/restore.sh\"\n",
                "  - 'curl http://169.254.169.254/latest/meta-data/public-keys/0/openssh-key >> /home/centos/.ssh/authorized_keys'\n",
                "  - 'rm -fr /etc/opt/jfrog/artifactory'\n",
                "  - 'ln -s /mnt/artifactory/etc /etc/opt/jfrog/artifactory'"
              ]
            ]
          }
        }
      },
      "DependsOn": "ArtRds"
    },
    "ArtASG": {
      "Type": "AWS::AutoScaling::AutoScalingGroup",
      "Properties": {
        "Cooldown": "300",
        "DesiredCapacity": "1",
        "MaxSize": "1",
        "MinSize": "1",
        "HealthCheckGracePeriod": "241",
        "HealthCheckType": "EC2",
        "VPCZoneIdentifier": [
          {
            "Ref" : "PrivateSubnet0"
          },
          {
            "Ref" : "PrivateSubnet1"
          },
          {
            "Ref" : "PrivateSubnet2"
          }
        ],
        "LaunchConfigurationName": {
          "Ref": "ArtLC"
        },
        "LoadBalancerNames": [
          {
            "Ref": "ArtElb"
          }
        ],
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [
              "-",
              [
                {
                  "Ref" : "ResourceLabel"
                },
                "artifactory-0"
              ]
            ] },
            "PropagateAtLaunch" : "true"
          }
        ]
      }
    },
    "ArtDBSubnets": {
      "Type": "AWS::RDS::DBSubnetGroup",
      "Properties": {
        "DBSubnetGroupDescription": "JFrog Artifactory DB Subnets",
        "SubnetIds": [
          {
            "Ref" : "PrivateSubnet0"
          },
          {
            "Ref" : "PrivateSubnet1"
          },
          {
            "Ref" : "PrivateSubnet2"
          }
        ],
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [
              "-",
              [
                {
                  "Ref" : "ResourceLabel"
                },
                "artifactory"
              ]
            ] }
          }
        ]
      }
    },
    "ArtDBParams" : {
      "Type" : "AWS::RDS::DBParameterGroup",
      "Properties" : {
        "Description" : "JFrog Artifactory DB Parameters",
        "Family" : "mysql5.6",
        "Parameters" : {
          "max_allowed_packet" : "67108864"
        },
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [
              "-",
              [
                {
                  "Ref" : "ResourceLabel"
                },
                "artifactory"
              ]
            ] }
          }
        ]
      }
    },
    "ArtRds": {
      "Type": "AWS::RDS::DBInstance",
      "Properties": {
        "AutoMinorVersionUpgrade": "true",
        "DBInstanceClass": {
          "Ref" : "DBInstanceType"
        },
        "Port": "3306",
        "AllocatedStorage": "30",
        "BackupRetentionPeriod": "7",
        "DBName": "start",
        "Engine": "mysql",
        "EngineVersion": "5.6.22",
        "LicenseModel": "general-public-license",
        "MasterUsername": "art",
        "MasterUserPassword": {
          "Ref" : "DBAdminPassword"
        },
        "PreferredBackupWindow": "00:03-00:33",
        "PreferredMaintenanceWindow": "mon:02:21-mon:02:51",
        "MultiAZ": "true",
        "StorageType" : "gp2",
        "VPCSecurityGroups": [
          {
            "Ref": "ArtSG"
          }
        ],
        "DBSubnetGroupName": {
          "Ref": "ArtDBSubnets"
        },
        "DBParameterGroupName" : {
          "Ref" : "ArtDBParams"
        },
        "Tags" : [
          {
            "Key" : "Name",
            "Value" : { "Fn::Join" : [
              "-",
              [
                {
                  "Ref" : "ResourceLabel"
                },
                "artifactory-rds"
              ]
            ] }
          }
        ]
      }
    }
  }
}
